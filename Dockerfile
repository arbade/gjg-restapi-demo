FROM maven:3.6.3-jdk-11 as build

EXPOSE 8080

ADD target/gjg.jar gjg.jar

ENTRYPOINT ["java","-jar","/gjg.jar"]
